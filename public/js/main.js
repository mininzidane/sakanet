$(function () {
	var $body = $('body');

	// send author's feedback
	$body.on('click', '.js-send-feedback', function () {
		var $this = $(this),
			$target = $($this.data('target'));

		$.post($target.data('url'), {
			uploaderId: $target.data('uploaderId'),
			text: $target.text()
		}, function (data) {
			if (data) {
				$this.add($target).prop('disabled', true);
				alert('Successful');
			} else {
				alert('Error');
			}
		});
		return false;
	});

	// fancy box
	$('.js-fancybox').fancybox();

	// rating plugin
	$('.js-rate-it').rateit({
		step: 1,
		min: 0,
		max: 5,
		resetable: false
	});

	$body.on('click', '.js-fancybox-open', function () {
		var $target = $($(this).data('target'));
		$.fancybox($target);
		return false;
	});

	$body.on('rated', '.js-rate-it', function () {
		var $this = $(this),
			rating = $this.rateit('value');

		$.get($this.data('url'), {
			videoId: $this.data('video-id'),
			value: rating
		});
	});

	$('.js-video').on('play', function (e) {
		sendVideoViewed($(this).data('id'));
	});

	$body.on('click', '.js-card-pay', function () {
		var $this = $(this),
			url = $this.data('url');

		$.fancybox.showLoading();
		$.get(url, function (result) {
			$.fancybox.hideLoading();
			if (result == 1) { // success

			} else {

			}
		});
		return false;
	});

	$('.js-datepicker').datepicker({
		autoclose: true,
		language: 'ja'
	});

	// playback rate
	$body.on('click', '.js-video-speed', function () {
		var $this = $(this),
			videoId = $this.data('video-id'),
			video = $('[data-id = ' + videoId + ']:visible').get(0);

		$('.js-video-speed[data-video-id = ' + videoId + ']').removeClass('active');
		$this.addClass('active');

		video.playbackRate = $this.data('rate');
		video.play();
		return false;
	});

	// play video from playlist
	var $playlist = $('.js-playlist').on('click', '[data-playlist-play]', function () {
		var $this = $(this),
			partId = $this.data('play-part'),
			videoId = $this.data('play-video-id'),
			currentPlaybackRate = $('.js-video-speed.active[data-video-id = ' + videoId + ']').data('rate'),
			prevVideo = $playlist.find('.playlist__video:visible video').get(0);

		// return video settings to initial
		prevVideo.pause();

		// show and play selected video
		var newVideo = $playlist
			.find('[data-video-part]').hide()
			.filter('[data-video-part = ' + partId + ']').show()
			.find('video').get(0);
		newVideo.playbackRate = currentPlaybackRate;
		newVideo.play();

		$playlist.find('tr').removeClass('active');
		$this.closest('tr').addClass('active');
		return false;
	});
});

function sendVideoViewed(videoId) {
	$.get('/ajax/set-video-viewed/', {
		videoId: videoId
	})
}
$(function () {
	var $body = $('body');

	// plus button
	$body.on('click', '.js-clone-row', function () {
		var $this = $(this),
			$target = $($this.data('target')).last();

		$target.clone().insertAfter($target).find(':input').val('');
		return false;
	});

	// minus button
	$body.on('click', '.js-delete-row', function () {
		var $this = $(this),
			$target = $($this.data('target')).last();

		$target.remove();
		return false;
	});

	$body.on('click', '.js-confirm', function () {
		return confirm($(this).data('confirm-text'));
	});

	$('.js-datepicker').datepicker({
		autoclose: true,
		language: 'ja'
	});
});
<?php
use yii\helpers\Html;

use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use app\assets\AdminAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AdminAsset::register($this);

$action = Yii::$app->controller->action->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php if (!Yii::$app->user->isGuest) { ?>
<div class="container" style="padding-top: 15px">
	<?php
	$actionId = Yii::$app->controller->action->id;
	echo Nav::widget([
		'options' => ['class' => 'nav-tabs'],
		'items'   => [
			[
				'label' => Yii::t('common', 'Site'),
				'url'   => ['main/index'],
			],
			[
				'label'  => Yii::t('common', 'Viewer list'),
				'url'    => ['admin/viewer-list'],
				'active' => strpos($actionId, 'viewer') === 0,
			],
			[
				'label'  => Yii::t('common', 'Uploader list'),
				'url'    => ['admin/uploader-list'],
				'active' => strpos($actionId, 'uploader') === 0 || in_array($actionId, ['video-add', 'video-edit']),
			],
			[
				'label'  => Yii::t('common', 'Video category list'),
				'url'    => ['admin/video-category-list'],
				'active' => strpos($actionId, 'video-category') === 0,
			],
		],
	]); ?>
</div>
<?php } ?>

<?php $this->beginBody() ?>
<div class="content" style="padding-top: 40px;">
	<div class="container">
		<?= $content ?>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

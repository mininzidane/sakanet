<?php
use yii\helpers\Html;

use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$action = Yii::$app->controller->action->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<?php if (!Yii::$app->user->isGuest) { ?>
	<div class="top-menu">
		<div class="container">
		</div>
	</div>
<?php } ?>
<header>
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<a href="/" class="logo"></a>
			</div>
			<div class="col-sm-8">
				<div class="row white-bg">
					<?php if (Yii::$app->user->isGuest) { ?>
						<!--<div class="col-sm-3">
							<div class="search-form">
								<form action="">
									<input type="text" name="" id="" class="form-control form-control_search"
										   placeholder="動画を検索する">
								</form>
							</div>
						</div>-->
						<div class="col-sm-12">
							<form action="<?= Url::to(['main/login']) ?>" class="login-form__form" method="post">
								<div class="row">
									<div class="col-sm-4">
										<input class="form-control form-control_login-form" type="text"
										       name="LoginForm[username]" placeholder="ID">
									</div>
									<div class="col-sm-4">
										<input class="form-control form-control_login-form form-control_password"
										       type="password" name="LoginForm[password]" placeholder="パスワード">
									</div>
									<div class="col-sm-4">
										<button type="submit"
										        class="btn login-form__button"><?= Yii::t('common', 'Login') ?></button>
									</div>
								</div>
							</form>
						</div>
					<?php } else { ?>
						<?php
						/** @var \app\models\User|\app\models\Admin $user */
						$user = Yii::$app->user->getIdentity();
						$isAdmin = $user->role == \app\models\Admin::ROLE;
						$colNumber = $isAdmin? 4: 6;
						?>
						<div class="col-sm-<?= $colNumber ?>">
							<div class="login-form__login">
								<?= $user->getLogin(); ?>
							</div>
						</div>
						<div class="col-sm-<?= $colNumber ?>">
							<a href="<?= Url::to(['main/logout']) ?>"
							   class="btn login-form__button"><?= Yii::t('common', 'Logout') ?></a>
						</div>
						<?php if ($isAdmin) { ?>
							<div class="col-sm-4">
								<?= Html::a(Yii::t('common', 'Admin'), ['admin/index'], ['class' => 'btn login-form__button']); ?>
							</div>
						<?php } ?>
					<?php } ?>
				</div>

				<?php if (Yii::$app->user->isGuest) { ?>
					<div class="login-form row">
						<div class="login-form__blue">
							<a href="<?= Url::to(['main/registration']) ?>">新規会員登録</a>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<nav class="header-nav">
		<div class="container">
			<a href="/" class="active">TOP</a>
			<a href="<?= Url::to(['main/new-video']) ?>">新着動画</a>
			<a href="<?= Url::to(['main/new-data']) ?>">新着資料</a>
			<a href="/blog/">ブログ</a>
			<a href="<?= Url::to(['main/seminar']) ?>">セミナー</a>
			<a href="<?= Url::to(['main/my-page']) ?>">マイページ</a>
		</div>
	</nav>
</header>
<div class="content">
	<div class="container">
		<?= $content ?>
	</div>
</div>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<a href="/" class="logo"></a>
			</div>
			<div class="col-sm-8">
				<nav class="footer-nav">
					<a href="#">ヘルプ</a>
					<a href="#">利用規約</a>
					<a href="#">運営会社</a>
					<a href="#">お問い合わせ</a>
				</nav>
			</div>
		</div>
		<div class="copyright">
			Copyright 2016 Movie Site.Inc
		</div>
	</div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

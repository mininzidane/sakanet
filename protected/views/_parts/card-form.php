<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var \app\models\User $model
 * @var yii\web\View     $this
 */
$card = $model->card;

$this->registerJsFile('https://js.stripe.com/v2/');
$stripeKey = Yii::$app->params['stripePublicKey'];
$js = <<< JS
	$(function() {
		Stripe.setPublishableKey('{$stripeKey}');
		var form$ = $('#card-form').on('submit', function () {
			$.fancybox.showLoading();
			Stripe.card.createToken({
				number: $('#usercard-number').val(),
				cvc: $('.card-cvc').val(),
				exp_month: $('#usercard-month').val(),
				exp_year: $('#usercard-year').val(),
				name: $('#usercard-cvv').val()
			}, function (status, response) {
				$.fancybox.hideLoading();
				if (response.error) {
					$('#card-form-alert')
						.text(response.error.message)
						.show();
				} else {
					// token contains id, last4, and card type
					var token = response['id'];
					// insert the token into the form so it gets submitted to the server
					$('#usercard-stripetoken').val(token);
					// and submit
					form$.get(0).submit();
				}
			});
			return false;
		});
	});
JS;
$this->registerJs($js);
?>

<h2 class="text-center post-y-1"><?= Yii::t('common', 'Payment info') ?></h2>

<?php if ($model->userCardId) { ?>
	<p>Card saved. Customer ID <?= $model->card->customerId ?>.</p>
	<?php return; ?>
<?php } ?>

<div id="card-form-alert" class="alert alert-danger" style="display: none"><?= Yii::t('common', 'Error') ?></div>
<div>
	<?php $form = ActiveForm::begin([
		'id'          => 'card-form',
		'options'     => ['class' => 'form-vertical'],
		'fieldConfig' => [
			'template' => "<div class=\"row\"><div class=\"col-sm-4\">{label}</div>\n<div class=\"col-sm-4\">{input}{error}</div></div>",
		],
	]); ?>

	<?= $form->field($card, 'stripeToken', ['template' => '{input}'])->hiddenInput() ?>

	<?php /*$form->field($card, 'type', [
		'template' => '<div class="form-group">{input}{error}</div>',
	])->radioList($card::getTypes(), [
		'unselect'    => 0,
		'itemOptions' => [
			'labelOptions' => ['class' => 'radio-inline'],
		],
	])*/ ?>
	<?= $form->field($card, 'holderName') ?>
	<?= $form->field($card, 'number') ?>
	<?php $years = $card::getValidYears(); ?>
	<?= $form->field($card, 'year')->dropDownList(array_combine($years, $years)) ?>
	<?php $months = $card::getValidMonth(); ?>
	<?= $form->field($card, 'month')->dropDownList(array_combine($months, $months)) ?>
	<?= $form->field($card, 'cvv')->passwordInput() ?>

	<div class="form-group text-center">
		<?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary', 'name' => 'registration-button']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>
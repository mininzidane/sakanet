<?php
/**
 * @var \app\models\Video     $video
 * @var \app\models\VideoPart $part
 */
$hideControls = isset($hideControls)? $hideControls: false;
?>
<?php if ($video->checkIsAvailable()) { ?>
<video <?= $hideControls? '': 'controls' ?> class="js-video video" data-id="<?= $video->id ?>" id="<?= $video->id ?>" poster="<?= $part->preview ?>">
	<source src="<?= $part->source ?>"
	        type="video/<?= pathinfo($part->source, PATHINFO_EXTENSION) ?>">
</video>
<?php } else { ?>
	<div class="panel panel-danger">
		<div class="panel-heading"><h3 class="panel-title"><?= Yii::t('common', 'Video unavailable') ?></h3></div>
		<div class="panel-body"><?= Yii::t('common', 'Sorry, you must pay for this video') ?></div>
	</div>
<?php } ?>
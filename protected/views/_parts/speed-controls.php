<?php
/**
 * @var \app\models\Video $video
 */
?>
<div class="form-group">
	<button class="btn btn-primary js-video-speed" data-rate="1" data-video-id="<?= $video->id ?>">1X</button>
	<button class="btn btn-info js-video-speed" data-rate="1.5" data-video-id="<?= $video->id ?>">1.5X</button>
	<button class="btn btn-warning js-video-speed" data-rate="2" data-video-id="<?= $video->id ?>">2X</button>
</div>
<?php
use yii\helpers\Url;

/**
 * @var \app\models\Video $video
 * @var bool              $showAll           Whether to show all the parts of video
 * @var bool              $asLink            Whether to go to the video detail by click
 * @var bool              $hideSpeedControls Whether to show speed control buttons
 * @var bool              $hideDesc          Whether to show description below video. Hidden on homepage
 */

$showAll           = isset($showAll)? $showAll: false;
$asLink            = isset($asLink)? $asLink: false;
$hideSpeedControls = isset($hideSpeedControls)? $hideSpeedControls: false;
$hideDesc          = isset($hideDesc)? $hideDesc: false;
?>
	<div
		class="movie-item" <?= $asLink? 'onclick="location.href = \'' . Url::to(['main/video-detail', 'id' => $video->id]) . '\';"': '' ?>>
		<?php if (count($video->parts)) {
			if ($showAll) {
				foreach ($video->parts as $part) {
					echo $this->render('@parts/video-frame', [
						'video'        => $video,
						'part'         => $part,
						'hideControls' => $asLink,
					]);
				}
			} else {
				echo $this->render('@parts/video-frame', [
					'video'        => $video,
					'part'         => $video->parts[0],
					'hideControls' => $asLink,
				]);
			}
		}
		?>
		<div class="movie-item__title"><?= $video->title ?></div>
		<?php if (!$hideDesc) { ?>
			<div class="movie-item__desc"><?= $video->description ?></div>
		<?php } ?>
	</div>
<?php if (!$hideSpeedControls) {
	echo $this->render('@parts/speed-controls', [
		'video' => $video,
	]);
} ?>
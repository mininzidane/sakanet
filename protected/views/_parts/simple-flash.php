<?php
/**
 * @var string $flashId
 */
$flash = Yii::$app->session->getFlash($flashId);
if ($flash !== null) {
	if ($flash) { ?>
		<div class="alert alert-success"><?= Yii::t('common', 'Successful') ?></div>
	<?php } else { ?>
		<div class="alert alert-danger"><?= Yii::t('common', 'Error') ?></div>
	<?php }
}
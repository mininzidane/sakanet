<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Video;

/**
 * @var                             $this yii\web\View
 * @var \app\models\VideoCategory[] $categories
 **/

$this->title = Yii::t('common', 'My page');
?>
<h1 class="page-title"><?= $this->title ?> <a class="btn btn-info pull-right" href="<?= Url::to(['main/edit-profile']) ?>"><?= Yii::t('common', 'Edit profile') ?></a></h1>

<p><?= Yii::t('common', 'Hello') . ' ' . Yii::$app->user->getIdentity()->getLogin() ?> さん</p>

<h3><?= Yii::t('common', 'Recommended videos') ?></h3>
<div class="row">
	<?php foreach (Video::getRecommendedVideos() as $video) { ?>
		<div class="col-sm-3 form-group">
			<?= $this->render('@parts/movie-item', [
				'video' => $video,
			]) ?>
		</div>
	<?php } ?>
</div>


<h3><?= Yii::t('common', 'Popular videos') ?></h3>
<div class="row">
	<?php foreach (Video::getPopularVideos() as $video) { ?>
		<div class="col-sm-3 form-group">
			<?= $this->render('@parts/movie-item', [
				'video' => $video,
			]) ?>
		</div>
	<?php } ?>
</div>

<div class="panel panel-default">
	<div class="panel-heading"><?= Yii::t('common', 'Search by categories') ?></div>

	<div class="panel-body">
		<form action="<?= Url::to(['main/search-by-categories']) ?>">
			<div class="row">
				<?php foreach ($categories as $category) { ?>
					<div class="col-sm-3 form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="q[categories][]" value="<?= $category->id ?>">
								<?= $category->title ?>
							</label>
						</div>
					</div>
				<?php } ?>
			</div>

			<div class="text-center">
				<?= Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) ?>
			</div>
		</form>
	</div>
</div>
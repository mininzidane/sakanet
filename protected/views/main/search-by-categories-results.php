<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Video;

/**
 * @var                             $this yii\web\View
 * @var \app\models\Video[]         $videos
 **/

$this->title = Yii::t('common', 'Search results');
?>

<?php if (count($videos) == 0) {
	echo '<div class="alert alert-warning">' . Yii::t('common', 'Empty list') . '</div>';
	return;
} ?>

<div class="row">
	<?php foreach ($videos as $video) { ?>
		<div class="col-sm-3 form-group">
			<?= $this->render('@parts/movie-item', [
				'video' => $video,
			]) ?>
		</div>
	<?php } ?>
</div>
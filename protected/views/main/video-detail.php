<?php

use yii\helpers\Html;
use yii\helpers\Url;
use \app\models\User;

/**
 * @var                     $this yii\web\View
 * @var \app\models\Video   $video
 * */
$this->title = Yii::t('common', 'Video detail');
?>

	<div class="row">
		<div class="col-sm-6">
			<div class="playlist js-playlist">
				<?php foreach ($video->parts as $i => $part) { ?>
					<div class="playlist__video" style="<?= $i == 0? 'display: block;': '' ?>" data-video-part="<?= $part->id ?>">
						<?= $this->render('@parts/video-frame', [
							'video' => $video,
							'part'  => $part,
						]) ?>
					</div>
				<?php } ?>

				<table class="table table-condensed table-hover playlist__table">
					<?php foreach ($video->parts as $i => $part) { ?>
						<tr class="<?= $i == 0? 'active': '' ?>">
							<td>
								<button class="btn btn-default" data-playlist-play data-play-part="<?= $part->id ?>" data-play-video-id="<?= $video->id ?>">
									<span class="glyphicon glyphicon-play"></span>
								</button>
							</td>
							<td>
								<span><?= $part->title ?></span>
							</td>
							<td>
								<span><span class="glyphicon glyphicon-time"></span>&nbsp;<?= $part->duration ?></span>
							</td>
						</tr>
					<?php } ?>
				</table>
				<?= $this->render('@parts/speed-controls', [
					'video' => $video,
				]); ?>
			</div>
		</div>
		<div class="col-sm-6">
			<h2><?= $video->title ?></h2>
			<p><?= $video->uploader->title ?></p>
			<div>
				<?= $video->description ?>
			</div>
			<p><?= Yii::t('common', 'Payment') ?>
				: <?= $video->payment? '¥' . Yii::$app->formatter->asDecimal($video->payment, 0): Yii::t('common', 'Free') ?>
				<?php if (Yii::$app->user->can(User::ACTIONS) && !$video->checkIsAvailable()) { ?>
					<button class="btn btn-success js-fancybox-open"
					        data-target="#pay-popup"><?= Yii::t('common', 'Pay') ?></button>
				<?php } ?>
			</p>
			<p><?= Yii::t('common', 'Total length') ?>: <?= $video->getTotalDuration() ?></p>
			<p><?= Yii::t('common', 'Total parts') ?>: <?= $video->getPartsCount() ?></p>
			<p>
				<?= Yii::t('common', 'Rating') ?>:
				<span class="js-rate-it" data-rateit-readonly="true" data-rateit-value="<?= $video->rating ?>"></span>
				<?php if (Yii::$app->user->can(User::ACTIONS)) { ?>
					<button class="btn btn-info js-fancybox-open"
					        data-target="#rate-popup"><?= Yii::t('common', 'Rate') ?></button>
				<?php } ?>
			</p>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-8">
			<ul class="nav nav-tabs">
				<li class="active">
					<a class="dropdown-toggle" data-toggle="tab" href="#documents" role="button" aria-haspopup="true"
					   aria-expanded="false">
						<?= Yii::t('common', 'Documents') ?>
					</a>
				</li>
				<li>
					<a class="dropdown-toggle" data-toggle="tab" href="#feedback" role="button" aria-haspopup="true"
					   aria-expanded="false">
						<?= Yii::t('common', 'Feedback') ?>
					</a>
				</li>
			</ul>
			<div id="myTabContent" class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="documents">
					<?php foreach ($video->documents as $document) { ?>
						<p><a href="<?= Url::to(['download-document', 'id' => $document->id]) ?>" target="_blank"
						      class="btn btn-primary"><?= $document->getName() ?></a></p>
					<?php } ?>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="feedback">
					<div class="form-group">
						<textarea id="send-feedback-text" rows="6" class="form-control"
						          data-uploader-id="<?= $video->uploaderId ?>"
						          data-url="<?= Url::to(['ajax/send-feedback']) ?>"
						          placeholder="<?= Yii::t('common', 'Review') ?>"></textarea>
					</div>
					<button class="btn btn-primary js-send-feedback"
					        data-target="#send-feedback-text"><?= Yii::t('common', 'Send') ?></button>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<h3><?= Yii::t('common', 'Related videos') ?></h3>
			<?php foreach ($video->getRelatedVideos() as $relatedVideo) {
				echo $this->render('@parts/movie-item', [
					'video'             => $relatedVideo,
					'asLink'            => true,
					'hideSpeedControls' => true,
					'hideDesc'          => true,
				]);
			} ?>
		</div>
	</div>

<?php if (Yii::$app->user->can(User::ACTIONS)) { ?>
	<div style="display: none;" id="rate-popup">
		<div class="text-center">
			<h4 class="nobr"><?= Yii::t('common', 'Rate this video') ?></h4>
			<span class="js-rate-it" data-rateit-value="<?= @$video->userRate->rate ?>"
			      data-video-id="<?= $video->id ?>" data-url="<?= Url::to(['ajax/rate']) ?>"></span>
		</div>
	</div>

	<div style="display: none;" id="pay-popup" class="text-center">
		<h4><?= Yii::t('common', 'Pay with the card that has wound') ?></h4>
		<?php if (Yii::$app->user->getIdentity()->userCardId) { ?>
			<div class="form-group">
				<button class="btn btn-success js-card-pay"
				        data-url="<?= Url::to(['ajax/pay-with-card', 'amount' => $video->payment]) ?>"><?= Yii::t('common', 'Yes') ?></button>
				<button class="btn btn-danger" onclick="$.fancybox.close();"><?= Yii::t('common', 'No') ?></button>
			</div>
		<?php } else { ?>
			<h4><?= Yii::t('common', 'You have no registered card') ?></h4>
			<div><a class="btn btn-primary"
			        href="<?= Url::to(['main/edit-profile']) ?>"><?= Yii::t('common', 'Register a card') ?></a>
			</div>
		<?php } ?>
	</div>
<?php } ?>
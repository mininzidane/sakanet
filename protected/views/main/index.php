<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View                $this
 * @var \app\models\Video[]         $newVideos
 * @var \app\models\VideoCategory[] $categories
 */
$this->title = 'Homepage';
?>

<div class="row">
	<div class="col-sm-4">
		<div class="border-blue">
			<form action="<?= Url::to(['main/search-by-categories']) ?>">
				<div class="border-blue__header">
					<i class="icon-search"></i>
					<?= Yii::t('common', 'Search by categories') ?>
				</div>
				<div class="border-blue__body">
					<?php foreach ($categories as $category) { ?>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="q[categories][]" value="<?= $category->id ?>">
								<?= $category->title ?>
							</label>
						</div>
					<?php } ?>
				</div>
				<button type="submit" class="border-blue__footer">
					<i class="icon-search icon-search_white"></i>
					<?= Yii::t('common', 'Search') ?>
				</button>
			</form>
		</div>

		<div class="border-green">
			<div class="border-green__body">
				<p><i class="icon-message"></i></p>
				<p>最新のマーケティング情報を
					メールで配信中</p>

				<input type="text" placeholder="Your email" class="form-control grey-input">
			</div>
			<div class="border-green__footer">
				無料登録する
			</div>
		</div>
	</div>
	<div class="col-sm-8">
		<h1 class="page-title"><?= Html::encode($this->title) ?></h1>
		<div class="row text-center">
			<?php
			$i = 0;
			foreach ($newVideos as $video) {
				if (!count($video->parts)) {
					continue;
				}
				if ($i % 2 == 0 && $i > 0) {
					echo '</div><div class="row text-center">';
				}
				$i++;
				?>
				<div class="col-sm-6">
					<?= $this->render('@parts/movie-item', [
						'video'             => $video,
						'asLink'            => true,
						'hideSpeedControls' => true,
						'hideDesc'          => true,
					]) ?>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
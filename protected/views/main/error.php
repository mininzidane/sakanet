<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $error \Exception */

$this->title = 'Error ' . @$error->statusCode;
?>
<div class="site-error">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php if ($error) { ?>
		<div class="alert alert-danger">
			<?= nl2br(Html::encode($error->getMessage())) ?>
		</div>
	<?php } ?>

	<p>
		<?= Yii::t('common', 'The above error occurred while the Web server was processing your request.'); ?>
	</p>
	<p>
		<?= Yii::t('common', 'Please contact us if you think this is a server error. Thank you.'); ?>
	</p>
</div>

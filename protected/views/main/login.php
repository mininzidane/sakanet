<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = Yii::t('common', 'Login');
?>

<?= $this->render('@parts/simple-flash', ['flashId' => 'registration']) ?>

<div class="site-login">
	<h1 class="page-title"><?= Html::encode($this->title) ?></h1>

	<?php $form = ActiveForm::begin([
		'id'          => 'login-form',
		'fieldConfig' => [
			'template' => "<div class=\"row\"><div class=\"col-sm-4\">{label}</div>\n<div class=\"col-sm-8\">{input}{error}</div></div>",
		],
	]); ?>

	<?= $form->field($model, 'username') ?>

	<?= $form->field($model, 'password')->passwordInput() ?>

	<?= $form->field($model, 'rememberMe', [
//        'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
	])->checkbox() ?>

	<div class="form-group">
		<?= Html::submitButton(Yii::t('common', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>

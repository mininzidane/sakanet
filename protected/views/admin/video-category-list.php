<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this       yii\web\View
 * @var $categories \app\models\VideoCategory[]
 */
$this->title = Yii::t('common', 'Category list'); ?>

<?= $this->render('@parts/simple-flash', ['flashId' => 'video-category']) ?>

<div class="form-group">
	<a href="<?= Url::to(['admin/video-category-add']) ?>" class="btn btn-success btn-lg"><?= Yii::t('common', 'Add') ?></a>
</div>

<?php if (count($categories) == 0) {
	echo '<div class="alert alert-warning">' . Yii::t('common', 'Empty list') . '</div>';
	return;
} ?>

<table class="table">
	<tr>
		<th></th>
		<th><?= $categories[0]->getAttributeLabel('title') ?></th>
		<th><?= $categories[0]->getAttributeLabel('created') ?></th>
		<th></th>
	</tr>
	<?php foreach ($categories as $category) { ?>
		<tr>
			<td><?= $category->id ?></td>
			<td><?= $category->title ?></td>
			<td><?= $category->created ?></td>
			<td>
				<a href="<?= Url::to(['admin/video-category-edit', 'id' => $category->id]) ?>" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span></a>
				<a href="<?= Url::to(['admin/model-delete', 'model' => \yii\helpers\StringHelper::basename($category::className()), 'id' => $category->id, 'redirect' => Url::to(['video-category-list'])]) ?>"
				   class="btn btn-danger js-confirm" data-confirm-text="<?= Yii::t('common', 'Are you sure?') ?>"><span class="glyphicon glyphicon-remove"></span></a>
			</td>
		</tr>
	<?php } ?>
</table>

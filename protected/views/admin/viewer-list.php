<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this  yii\web\View
 * @var $users \app\models\User[]
 */
$this->title = Yii::t('common', 'Viewer list'); ?>

<?= $this->render('@parts/simple-flash', ['flashId' => 'delete-user']) ?>

<div class="form-group">
	<a href="<?= Url::to(['admin/viewer-add']) ?>" class="btn btn-success btn-lg"><?= Yii::t('common', 'Add') ?></a>
</div>

<?php if (count($users) == 0) {
	echo '<div class="alert alert-warning">' . Yii::t('common', 'Empty list') . '</div>';
	return;
} ?>

<table class="table">
	<tr>
		<th></th>
		<th><?= $users[0]->getAttributeLabel('firstName') ?></th>
		<th><?= $users[0]->getAttributeLabel('firstNameWrite') ?></th>
		<th><?= $users[0]->getAttributeLabel('email') ?></th>
		<th><?= $users[0]->getAttributeLabel('address') ?></th>
		<th><?= Yii::t('common', 'Date of birth') ?></th>
		<th><?= $users[0]->getAttributeLabel('occupation') ?></th>
		<th></th>
	</tr>
	<?php foreach ($users as $user) { ?>
		<tr>
			<td><?= $user->id ?></td>
			<td><?= $user->firstName ?></td>
			<td><?= $user->firstNameWrite ?></td>
			<td><?= $user->email ?></td>
			<td><?= implode(', ', array_filter([$user->city, $user->address, $user->zip])) ?></td>
			<td><?= $user->birthday ?></td>
			<td><?= $user->occupation ?></td>
			<td>
				<a href="<?= Url::to(['admin/viewer-edit', 'id' => $user->id]) ?>" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span></a>
				<a href="<?= Url::to(['admin/model-delete', 'id' => $user->id]) ?>" class="btn btn-danger js-confirm" data-confirm-text="<?= Yii::t('common', 'Are you sure?') ?>"><span class="glyphicon glyphicon-remove"></span></a>
			</td>
		</tr>
	<?php } ?>
</table>

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this  yii\web\View
 * @var $user  \app\models\User
 */
$this->title = Yii::t('common', 'Viewer edit'); ?>

<?= $this->render('@parts/simple-flash', ['flashId' => Yii::$app->controller->action->id]) ?>

<?php $form = ActiveForm::begin([
	'id'          => 'user-edit-form',
	'options'     => ['class' => 'form-vertical'],
	'fieldConfig' => [
		'template' => "<div class=\"row\"><div class=\"col-sm-4\">{label}</div>\n<div class=\"col-sm-8\">{input}{error}</div></div>",
	],
]); ?>

<?= $form->field($user, 'firstName') ?>
<?= $form->field($user, 'firstNameWrite') ?>
<?= $form->field($user, 'password')->passwordInput(['value' => '']) ?>
<?= $form->field($user, 'email') ?>
<?= $form->field($user, 'zip') ?>
<?= $form->field($user, 'prefecture') ?>
<?= $form->field($user, 'city') ?>
<?= $form->field($user, 'address') ?>
<?= $form->field($user, 'occupation') ?>
<?= $form->field($user, 'birthday', ['inputOptions' => ['class' => 'form-control js-datepicker']]) ?>

<div class="form-group">
	<?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
	<?= Html::a(Yii::t('common', 'Back to list'), ['admin/viewer-list'], ['class' => 'btn btn-link']) ?>
</div>

<?php ActiveForm::end(); ?>

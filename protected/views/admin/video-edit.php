<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;
use \yii\helpers\StringHelper;

/**
 * @var $this   yii\web\View
 * @var $model  \app\models\Video
 */
$this->title = Yii::t('common', 'Video edit');
?>

<?php $form = ActiveForm::begin([
	'options'     => ['enctype' => 'multipart/form-data', 'class' => 'form-vertical'],
	'fieldConfig' => [
		'template' => "<div class=\"row\"><div class=\"col-sm-5\">{label}</div>\n<div class=\"col-sm-7\">{input}{error}</div></div>",
	],
]); ?>

<div class="form-group">
	<?php if (count($model->parts)) { ?>
		<table class="table table-condensed">
			<tr>
				<th><?= $model->parts[0]->getAttributeLabel('title') ?></th>
				<th><?= $model->parts[0]->getAttributeLabel('duration') ?></th>
				<th><?= $model->parts[0]->getAttributeLabel('preview') ?></th>
				<th></th>
			</tr>
			<?php foreach ($model->parts as $videoPart) { ?>
				<tr>
					<td><?= $videoPart->title ?></td>
					<td><?= $videoPart->duration ?></td>
					<td><?= Html::img($videoPart->preview, ['alt' => $videoPart->preview]) ?></td>
					<td class="text-right"><?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['admin/model-delete', 'id' => $videoPart->id, 'model' => StringHelper::basename($videoPart::className()), 'redirect' => Url::to(['video-edit', 'id' => $model->id])], ['class' => 'btn btn-sm btn-danger js-confirm', 'data-confirm-text' => Yii::t('common', 'Are you sure?') ]) ?></td>
				</tr>
			<?php } ?>
		</table>
	<?php } ?>
	<div class="row js-row-for-clone">
		<div class="col-sm-4">
			<?= $form->field($model, 'partTitles[]') ?>
		</div>
		<div class="col-sm-4">
			<?= $form->field($model, 'sources[]', ['template' => '{input}{error}'])
				->fileInput(['multiple' => true]) ?>
		</div>
		<div class="col-sm-4">
			<?= $form->field($model, 'durations[]') ?>
		</div>
		<div class="col-sm-4">
			<?= $form->field($model, 'previews[]')
				->fileInput(['multiple' => true]) ?>
		</div>
	</div>
	<div class="form-group">
		<button class="btn btn-primary js-clone-row" data-target=".js-row-for-clone">
			<span class="glyphicon glyphicon-plus"></span>
		</button>
		<button class="btn btn-danger js-delete-row" data-target=".js-row-for-clone">
			<span class="glyphicon glyphicon-minus"></span>
		</button>
	</div>
</div>

<div class="row">
	<div class="col-sm-8">
		<div class="form-group">
			<?= $form->field($model, 'title') ?>
			<?= $form->field($model, 'description')->textarea(['rows' => 8]) ?>
			<?= $form->field($model, 'payment') ?>
			<?= $form->field($model, 'categoryId')->dropDownList(ArrayHelper::map(\app\models\VideoCategory::find()->all(), 'id', 'title')) ?>
		</div>

		<table class="table table-condensed">
			<?php foreach ($model->documents as $document) { ?>
				<tr>
					<td><?= $document->getName() ?></td>
					<td class="text-right"><?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['admin/model-delete', 'id' => $document->id, 'model' => StringHelper::basename($document::className()), 'redirect' => Url::to(['video-edit', 'id' => $model->id])], ['class' => 'btn btn-sm btn-danger js-confirm', 'data-confirm-text' => Yii::t('common', 'Are you sure?') ]) ?></td>
				</tr>
			<?php } ?>
		</table>
		<?php
		for ($i = 0; $i < $model::MAX_DOCUMENTS - count($model->documents); $i++) {
			echo $form->field($model, 'documentFiles[]')->fileInput(['multiple' => true]);
		}
		?>
	</div>
</div>

<div class="form-group">
	<?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
	<?= Html::a(Yii::t('common', 'Back to list'), ['admin/uploader-list'], ['class' => 'btn btn-link']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\helpers\Url;

/**
 * @var $this  yii\web\View
 * @var $user  \app\models\Uploader
 */
$this->title = Yii::t('common', 'Uploader detail');
?>

<div class="text-right">
	<a href="<?= Url::to(['admin/uploader-edit', 'id' => $user->id]) ?>" class="btn btn-success"><?= Yii::t('common', 'Edit') ?></a>
</div>

<div class="form-group">
	<b><?= $user->getAttributeLabel('title') ?></b> : <?= $user->title ?>
</div>
<div class="form-group">
	<b><?= $user->getAttributeLabel('contactName') ?></b> : <?= $user->contactName ?>
</div>
<div class="form-group">
	<b><?= $user->getAttributeLabel('mail') ?></b> : <a href="mailto:<?= $user->mail ?>"><?= $user->mail ?></a>
</div>
<div class="form-group">
	<b><?= $user->getAttributeLabel('phone') ?></b> : <?= $user->phone ?>
</div>
<div class="form-group">
	<b><?= $user->getAttributeLabel('address') ?></b> : <?= $user->address ?>
</div>
<div class="form-group">
	<b><?= Yii::t('common', 'Upload') ?></b> : <?= $user->getVideosCount() ?>
</div>

<?php if (count($user->videos)) { ?>
<table class="table table-hover">
	<tr>
		<th></th>
		<th><?= $user->videos[0]->getAttributeLabel('title') ?></th>
		<th><?= $user->videos[0]->getAttributeLabel('rating') ?></th>
		<th><?= Yii::t('common', 'count') ?></th>
		<th><?= Yii::t('common', 'time') ?></th>
		<th></th>
	</tr>
	<?php foreach ($user->videos as $video) { ?>
		<tr>
			<td><?= $video->id ?></td>
			<td><?= $video->title ?></td>
			<td><?= $video->rating ?></td>
			<td><?= $video->getPartsCount() ?></td>
			<td><?= $video->getTotalDuration() ?></td>
			<td>
				<a href="<?= Url::to(['admin/video-edit', 'id' => $video->id]) ?>" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span></a>
				<a href="<?= Url::to(['admin/model-delete', 'id' => $video->id, 'model' => 'video', 'redirect' => Url::to(['admin/uploader-detail', 'id' => $user->id])]) ?>" class="btn btn-danger js-confirm" data-confirm-text="<?= Yii::t('common', 'Are you sure?') ?>"><span class="glyphicon glyphicon-remove"></span></a>
			</td>
		</tr>
	<?php } ?>
</table>
<?php } ?>

<div class="text-left">
	<a href="<?= Url::to(['admin/video-add', 'uploaderId' => $user->id]) ?>" class="btn btn-primary"><?= Yii::t('common', 'Upload') ?></a>
</div>

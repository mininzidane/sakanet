<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this  yii\web\View
 * @var $user  \app\models\Uploader
 */
$this->title = Yii::t('common', 'Uploader edit');
?>

<?php $form = ActiveForm::begin([
	'id'          => 'user-edit-form',
	'options'     => ['class' => 'form-vertical'],
	'fieldConfig' => [
		'template' => "<div class=\"row\"><div class=\"col-sm-4\">{label}</div>\n<div class=\"col-sm-8\">{input}{error}</div></div>",
	],
]); ?>

<?= $form->field($user, 'title') ?>
<?= $form->field($user, 'contactName') ?>
<?= $form->field($user, 'mail') ?>
<?= $form->field($user, 'phone') ?>
<?= $form->field($user, 'address') ?>

<div class="form-group">
	<?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
	<?= Html::a(Yii::t('common', 'Back to list'), ['admin/uploader-list'], ['class' => 'btn btn-link']) ?>
</div>

<?php ActiveForm::end(); ?>

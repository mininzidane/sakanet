<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;
use \yii\helpers\StringHelper;

/**
 * @var $this   yii\web\View
 * @var $model  \app\models\VideoCategory
 */
$this->title = Yii::t('common', 'Video category edit');
?>

<?php $form = ActiveForm::begin([
	'options'     => ['class' => 'form-vertical'],
	'fieldConfig' => [
		'template' => "<div class=\"row\"><div class=\"col-sm-4\">{label}</div>\n<div class=\"col-sm-8\">{input}{error}</div></div>",
	],
]); ?>

<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'title') ?>
	</div>
</div>

<div class="form-group">
	<?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
	<?= Html::a(Yii::t('common', 'Back to list'), ['admin/video-category-list'], ['class' => 'btn btn-link']) ?>
</div>

<?php ActiveForm::end(); ?>

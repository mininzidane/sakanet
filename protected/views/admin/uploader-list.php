<?php
use yii\helpers\Html;
use yii\helpers\Url;
use \app\models\Uploader;

/**
 * @var $this  yii\web\View
 * @var $users \app\models\Uploader[]
 */
$this->title = Yii::t('common', 'Uploader list'); ?>

<?= $this->render('@parts/simple-flash', ['flashId' => 'uploader']) ?>

<div class="form-group">
	<a href="<?= Url::to(['admin/uploader-add']) ?>" class="btn btn-success btn-lg"><?= Yii::t('common', 'Add') ?></a>
</div>

<?php if (count($users) == 0) {
	echo '<div class="alert alert-warning">' . Yii::t('common', 'Empty list') . '</div>';
	return;
} ?>

<table class="table table-hover">
	<tr>
		<th></th>
		<th><?= $users[0]->getAttributeLabel('title') ?></th>
		<th><?= $users[0]->getAttributeLabel('contactName') ?></th>
		<th><?= Yii::t('common', 'Video count') ?></th>
		<th></th>
	</tr>
	<?php foreach ($users as $user) { ?>
		<tr>
			<td><?= $user->id ?></td>
			<td>
				<a href="<?= Url::to(['admin/uploader-detail', 'id' => $user->id]) ?>"><?= $user->title ?></a>
			</td>
			<td><?= $user->contactName ?></td>
			<td><?= $user->getVideosCount() ?></td>
			<td class="text-right">
				<a href="<?= Url::to(['admin/uploader-edit', 'id' => $user->id]) ?>" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span></a>
				<a href="<?= Url::to(['admin/model-delete', 'id' => $user->id, 'model' => 'uploader', 'redirect' => Url::to(['admin/uploader-list'])]) ?>" class="btn btn-danger js-confirm" data-confirm-text="<?= Yii::t('common', 'Are you sure?') ?>"><span class="glyphicon glyphicon-remove"></span></a>
			</td>
		</tr>
	<?php } ?>
</table>

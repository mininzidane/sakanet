<?php

namespace app\models;

/**
 * @property int $id
 * @property int $userId
 * @property int $rate
 * @property int $videoId
 */
class UserRate extends ActiveRecord {

	public static function tableName() {
		return 'user_rate_to_video';
	}

	public function rules() {
		return [
			[['userId', 'rate', 'videoId'], 'required'],
			['rate', 'in', 'range' => range(1, 5)],
		];
	}

	public function afterSave($insert, $changedAttributes) {
		parent::afterSave($insert, $changedAttributes);
		$video = Video::findOne($this->videoId);
		$video->recalculateRating();
	}
}
 
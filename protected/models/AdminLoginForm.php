<?php

namespace app\models;

use Yii;
use yii\base\Model;

class AdminLoginForm extends LoginForm {

	/**
	 * Finds user by [[username]]
	 *
	 * @return Admin|null
	 */
	public function getUser() {
		if ($this->_user === false) {
			$this->_user = Admin::findByUsername($this->username);
			$this->_user->role = Admin::ROLE;
		}

		return $this->_user;
	}
}

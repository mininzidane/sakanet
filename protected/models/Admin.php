<?php

namespace app\models;

/**
 * Class Admin
 * @package app\models
 *
 * @property int    $id
 * @property string $login
 * @property string $password
 */
class Admin extends User {

	const ACTIONS = 'adminActions';
	const ROLE = 'admin';

	public $role = self::ROLE;

	public function rules() {
		return [
			[['login', 'password'], 'required'],
		];
	}

	public function getLogin() {
		return $this->login;
	}

	public static function findByUsername($username) {
		return self::findOne(['login' => $username]);
	}
}
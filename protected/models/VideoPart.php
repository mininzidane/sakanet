<?php

namespace app\models;

/**
 * Class Uploader
 * @package app\models
 *
 * @property int    $id
 * @property string $title
 * @property string $source
 * @property string $preview
 * @property int    $duration
 * @property int    $videoId
 *
 * @property Video  $video
 */
class VideoPart extends ActiveRecord {

	public function rules() {
		return [
			[['title', 'source', 'videoId', 'duration'], 'required'],
			[['preview'], 'safe'],
		];
	}

	public function getVideo() {
		return $this->hasOne(Video::className(), ['id' => 'videoId']);
	}

	public function beforeDelete() {
		if (parent::beforeDelete()) {
			@unlink(\Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $this->source);
			return true;
		} else {
			return false;
		}
	}

	public function afterFind() {
		parent::afterFind();
		$this->duration = gmdate('i:s', $this->duration);
	}
}
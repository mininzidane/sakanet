<?php

namespace app\models;

/**
 * Class Uploader
 * @package app\models
 *
 * @property int    $id
 * @property string $customerId
 * @property string $created
 */
class UserCard extends ActiveRecord {

	public $type;
	public $holderName;
	public $number;
	public $month;
	public $year;
	public $cvv;
	public $stripeToken;

	const TYPE_VISA             = 1;
	const TYPE_MASTERCARD       = 2;
	const TYPE_JCB              = 3;
	const TYPE_AMERICAN_EXPRESS = 4;

	public static function getTypes() {
		return [
			self::TYPE_VISA             => 'Visa',
			self::TYPE_MASTERCARD       => 'Master Card',
			self::TYPE_JCB              => 'JCB',
			self::TYPE_AMERICAN_EXPRESS => 'American Express',
		];
	}

	public static function getValidYears() {
		return range(date('Y'), date('Y') + 10);
	}

	public static function getValidMonth() {
		return range(1, 12);
	}

	public function rules() {
		return [
			[['holderName', 'number', 'month', 'year', 'cvv'], 'required'],
			[['stripeToken'], 'safe'],
		];
	}

	public function pay($amount) {
		\Stripe\Stripe::setApiKey(\Yii::$app->params['stripeSecretKey']);
		try {
			\Stripe\Charge::create([
				'amount'   => $amount,
				'currency' => 'jpy',
				'customer' => $this->customerId,
			]);
		} catch (\Stripe\Error\Card $e) {
			return $e->getMessage();
		} catch (\Exception $e) {
			return $e->getMessage();
		}
		return true;
	}
}
<?php

namespace app\models\traits;

trait AttributeLabelTrait {
	public function getAttributeLabel($attribute) {
		$label = parent::getAttributeLabel($attribute);
		return \Yii::t('common', $label);
	}
}
 
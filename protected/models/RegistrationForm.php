<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegistrationForm extends Model {

	use traits\AttributeLabelTrait;

	public $firstName;
	public $lastName;
	public $firstNameWrite;
	public $email;
	public $password;
	public $zip;
	public $prefecture;
	public $city;
	public $address;
	public $birthday;
	public $occupation;
	public $subscribe = 1;
	public $agree;

	public $year;
	public $month;
	public $day;

	public static function getPrefectures() {
		return [
			'愛知県',
			'秋田県',
			'青森県',
			'千葉県',
			'愛媛県',
			'福井県',
			'福岡県',
			'福島県',
			'岐阜県',
			'群馬県',
			'広島県',
			'北海道',
			'兵庫県',
			'茨城県',
			'石川県',
			'岩手県',
			'香川県',
			'鹿児島県',
			'神奈川県',
			'高知県',
			'熊本県',
			'京都府',
			'三重県',
			'宮城県',
			'宮崎県',
			'長野県',
			'長崎県',
			'奈良県',
			'新潟県',
			'大分県',
			'岡山県',
			'沖縄県',
			'大阪府',
			'佐賀県',
			'埼玉県',
			'滋賀県',
			'島根県',
			'静岡県',
			'栃木県',
			'徳島県',
			'東京都',
			'鳥取県',
			'富山県',
			'和歌山県',
			'山形県',
			'山口県',
			'山梨県',
		];
	}

	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		return [
			// name, email, subject and body are required
			[['email', 'password', 'agree'], 'required'],
			['password', 'string', 'min' => 4],
			// email has to be a valid email address
			['email', 'email'],
			// verifyCode needs to be entered correctly
			['agree', 'in', 'range' => [1]],
			[[
				'firstNameWrite',
				'firstName',
				'year',
				'month',
				'day',
				'zip',
				'zip',
				'prefecture',
				'prefecture',
				'city',
				'address',
				'occupation',
			], 'safe']
		];
	}

	public function createUser() {
		$user                 = new User();
		$user->firstName      = $this->firstName;
		$user->firstNameWrite = $this->firstNameWrite;
		$user->email          = $this->email;
		$user->password       = $this->password;
		$user->zip            = $this->zip;
		$user->prefecture     = $this->prefecture;
		$user->city           = $this->city;
		$user->address        = $this->address;
		$user->birthday       = implode('/', [$this->year, str_pad($this->month, 2, 0, STR_PAD_LEFT), str_pad($this->day, 2, 0, STR_PAD_LEFT)]);
		$user->occupation     = $this->occupation;
		$user->subscribe      = $this->subscribe;
		return $user;
	}
}

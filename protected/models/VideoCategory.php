<?php

namespace app\models;

/**
 * Class Uploader
 * @package app\models
 *
 * @property int    $id
 * @property string $title
 * @property string $created
 */
class VideoCategory extends ActiveRecord {

	public function rules() {
		return [
			[['title'], 'required'],
		];
	}
}
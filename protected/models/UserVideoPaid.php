<?php

namespace app\models;

/**
 * @property int $userId
 * @property int $videoId
 */
class UserVideoPaid extends ActiveRecord {

	public static function tableName() {
		return 'user_video_paid';
	}

	public function rules() {
		return [
			[['userId', 'videoId'], 'required'],
		];
	}
}
 
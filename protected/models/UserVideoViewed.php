<?php

namespace app\models;

/**
 * @property int $userId
 * @property int $videoId
 */
class UserVideoViewed extends ActiveRecord {

	public static function tableName() {
		return 'user_video_viewed';
	}

	public function rules() {
		return [
			[['userId', 'videoId'], 'required'],
		];
	}
}
 
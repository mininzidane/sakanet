<?php

namespace app\models;

use yii\image\drivers\Image_GD;
use yii\web\UploadedFile;
use \Yii;

/**
 * Class Uploader
 * @package app\models
 *
 * @property int             $id
 * @property string          $title
 * @property int             $uploaderId
 * @property string          $description
 * @property int             $categoryId
 * @property string          $payment
 * @property string          $rating
 *
 * @property VideoDocument[] $documents
 * @property VideoPart[]     $parts
 * @property Uploader        $uploader
 * @property VideoCategory   $category
 * @property UserRate        $userRate
 */
class Video extends ActiveRecord {

	const MAX_DOCUMENTS = 3;

	/** @var array */
	public $partTitles;
	/** @var UploadedFile[] */
	public $sources;
	/** @var UploadedFile[] */
	public $previews;
	/** @var int[] */
	public $durations;
	/** @var UploadedFile[] */
	public $documentFiles;

	public function rules() {
		return [
			[['title', 'uploaderId'], 'required'],
			[['partTitles', 'durations'], 'required', 'skipOnEmpty' => true],
			[['description', 'durations', 'partTitles', 'categoryId', 'payment'], 'safe'],
			['sources', 'file', 'extensions' => 'mp4, png', 'maxFiles' => 0],
			['documentFiles', 'file', 'maxFiles' => self::MAX_DOCUMENTS],
		];
	}

	public function getDocuments() {
		return $this->hasMany(VideoDocument::className(), ['videoId' => 'id']);
	}

	public function getParts() {
		return $this->hasMany(VideoPart::className(), ['videoId' => 'id']);
	}

	public function getUploader() {
		return $this->hasOne(Uploader::className(), ['id' => 'uploaderId']);
	}

	public function getCategory() {
		return $this->hasOne(VideoCategory::className(), ['id' => 'categoryId']);
	}

	public function getUserRate() {
		$rate = UserRate::findOne([
			'userId'  => \Yii::$app->user->getCleanId(),
			'videoId' => $this->id,
		]);
		return $rate;
	}

	public function getPartsCount() {
		$count = (new \yii\db\Query())
			->select('COUNT(*)')
			->from(VideoPart::tableName())
			->where('videoId = :videoId', [':videoId' => $this->id])
			->scalar();
		return $count;
	}

	public function getTotalDuration() {
		$count = (new \yii\db\Query())
			->select('SUM(duration)')
			->from(VideoPart::tableName())
			->where('videoId = :videoId', [':videoId' => $this->id])
			->scalar();
		return $count;
	}

	private function savePart($title, $source, $duration, $previewSource = null) {
		if (!$part = VideoPart::findOne(['source' => $source, 'videoId' => $this->id])) {
			$part = new VideoPart();
		}
		$part->attributes = [
			'title'    => $title,
			'source'   => $source,
			'preview'  => $previewSource,
			'duration' => $duration,
			'videoId'  => $this->id,
		];
		$success       = $part->save();
		if (!$success) {
			foreach ($part->errors as $errorLabel => $errors) {
				switch ($errorLabel) {
					case 'duration':
						$this->addErrors(['durations' => $errors]);
						break;
					case 'title':
						$this->addErrors(['partTitles' => $errors]);
						break;
				}
			}
		}
		return $success;
	}

	public function saveParts() {
		$success        = false;
		$this->sources  = UploadedFile::getInstances($this, 'sources');
		$this->previews = UploadedFile::getInstances($this, 'previews');
		$folder         = $this->getUploadFolder();
		if (!file_exists(\Yii::getAlias('@webroot') . $folder)) {
			mkdir(\Yii::getAlias('@webroot') . $folder, 0755, true);
		}
		foreach ($this->sources as $i => $source) {
			$preview         = $this->previews[$i];
			$previewFileName = $folder . md5_file($preview->tempName) . '.' . $preview->extension;
			/** @var Image_GD $image */
			$image = Yii::$app->image->load($preview->tempName);
			$image->resize(Yii::$app->params['previewImageWidth']);

			$fileName        = $folder . md5_file($source->tempName) . '.' . $source->extension;
			$success         = $source->saveAs(substr($fileName, 1))
				&& $image->save(Yii::getAlias('@webroot') . $previewFileName)
				&& $this->savePart($this->partTitles[$i], $fileName, $this->durations[$i], $previewFileName);
		}
		return $success; // LOL only for last operation
	}

	public function getUploadFolder() {
		return \Yii::$app->params['videoFolder'] . $this->id . DIRECTORY_SEPARATOR;
	}

	/**
	 * @param string $source
	 * @param string $title
	 * @return bool
	 */
	private function saveDocument($source, $title) {
		if (VideoDocument::findOne(['source' => $source, 'videoId' => $this->id])) {
			return false;
		}
		$document             = new VideoDocument();
		$document->attributes = [
			'source'  => $source,
			'title'   => $title,
			'videoId' => $this->id,
		];
		return $document->save();
	}

	public function saveDocuments() {
		$success             = false;
		$this->documentFiles = UploadedFile::getInstances($this, 'documentFiles');
		$folder              = $this->getUploadFolder();
		if (!file_exists(\Yii::getAlias('@webroot') . $folder)) {
			mkdir(\Yii::getAlias('@webroot') . $folder, 0755, true);
		}
		foreach ($this->documentFiles as $documentFiles) {
			$fileName = $folder . md5_file($documentFiles->tempName) . '.' . $documentFiles->extension;
			$success  = $documentFiles->saveAs(substr($fileName, 1)) && $this->saveDocument($fileName, $documentFiles->name);
		}
		return $success;
	}

	public function beforeDelete() {
		if (parent::beforeDelete()) {
			foreach ($this->parts as $part) {
				$part->delete();
			}
			foreach ($this->documents as $document) {
				$document->delete();
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param int|null $limit
	 * @return static[]
	 */
	public function getRelatedVideos($limit = null) {
		return self::find()
			->limit($limit)
			->where('categoryId = :categoryId AND id != :id', [
				':categoryId' => $this->categoryId,
				':id'         => $this->id,
			])
			->all();
	}

	/**
	 * @param int|null $limit
	 * @return static[]
	 */
	public static function getPopularVideos($limit = null) {
		return self::find()
			->limit($limit)
			->where('rating = 5')
			->all();
	}

	/**
	 * @return static[]
	 */
	public static function getRecommendedVideos() {
		$userId = \Yii::$app->user->getCleanId();
		$alias  = self::tableName();
		$videos = self::find()
			->leftJoin(UserVideoViewed::tableName() . ' viewed', "{$alias}.id = viewed.videoId AND viewed.userId = :userId", [':userId' => $userId])
			->where('rating >= 4 AND viewed.videoId IS NULL')
			->all();
		return $videos;
	}

	public function recalculateRating() {
		$rating = (float) \Yii::$app->db
			->createCommand('SELECT SUM(`rate`) / COUNT(*) FROM ' . UserRate::tableName() . ' WHERE `videoId` = :videoId', [':videoId' => $this->id])
			->queryScalar();

		$this->rating = round($rating, 2);
		$this->save(false, ['rating']);
	}

	private function checkIsPaid() {
		return UserVideoPaid::find()->where([
			'userId'  => \Yii::$app->user->id,
			'videoId' => $this->id,
		])->exists();
	}

	public function checkIsAvailable() {
		return true; // todo delete line
		return !\Yii::$app->user->isGuest && (!$this->payment || $this->checkIsPaid());
	}
}

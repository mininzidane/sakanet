<?php

namespace app\models;

use \Yii;

/**
 * Class Uploader
 * @package app\models
 *
 * @property int    $id
 * @property string $title
 * @property string $contactName
 * @property string $mail
 * @property string $phone
 * @property string $address
 * @property string $created
 *
 * @property Video[] $videos
 */
class Uploader extends ActiveRecord {

	public function rules() {
		return [
			[['title'], 'required'],
			['mail', 'email'],
			[['contactName', 'phone', 'address'], 'safe'],
		];
	}

	public function getVideosCount() {
		$count = (new \yii\db\Query())
			->select('COUNT(*)')
			->from(Video::tableName())
			->where('uploaderId = :uploaderId', [':uploaderId' => $this->id])
			->scalar();
		return $count;
	}

	public function getVideos() {
		return $this->hasMany(Video::className(), ['uploaderId' => 'id']);
	}
}
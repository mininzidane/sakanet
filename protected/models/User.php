<?php

namespace app\models;

use yii\db\ActiveQuery;

/**
 * Class User
 * @package app\models
 *
 * @property int      $id
 * @property string   $firstName
 * @property string   $firstNameWrite
 * @property string   $email
 * @property string   $password
 * @property int      $userCardId
 * @property string   $zip
 * @property string   $prefecture
 * @property string   $city
 * @property string   $address
 * @property string   $occupation
 * @property string   $birthday
 * @property string   $subscribe
 * @property string   $created
 *
 * @property UserCard $card
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface {

	const ROLE         = 'user';
	const ACTIONS      = 'userActions';
	const ID_SEPARATOR = '-';
	const SALT         = 'salt_me_softly';

	public $authKey;
	public $accessToken;
	public $role = self::ROLE;

	public function rules() {
		$rules = [
			[['email'], 'required'],
			['email', 'email'],
			[[
				'firstName',
				'firstNameWrite',
				'zip',
				'prefecture',
				'city',
				'address',
				'occupation',
				'birthday',
				'subscribe',
			], 'safe'],
		];
		if ($this->isNewRecord) {
			$rules[] = ['password', 'required'];
			$rules[] = ['password', 'string', 'min' => 4];
		}
		return $rules;
	}

	public static function find() {
		return new UserQuery(get_called_class());
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		$pieces = explode(self::ID_SEPARATOR, $id, 2);

		if ($pieces[0] == Admin::ROLE) {
			$model = Admin::findOne($pieces[1]);
		} else {
			$model = self::findOne($pieces[1]);
		}
		return $model;
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null) {

	}

	/**
	 * @inheritdoc
	 */
	public function getId() {
		return static::ROLE . self::ID_SEPARATOR . $this->id;
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey() {
		return $this->authKey;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey) {
		return $this->authKey === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param  string $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return $this->password === $this->getHashedPassword($password);
	}

	/**
	 * Return hashed password
	 * @param $password
	 * @return string
	 */
	public function getHashedPassword($password) {
		return md5(self::SALT . $password);
	}

	public function beforeSave($insert) {
		if (!parent::beforeSave($insert)) {
			return false;
		}

		if ($this->isAttributeChanged('password') && $this->password) {
			$this->password = $this->getHashedPassword($this->password);
		}
		$this->subscribe = (int) $this->subscribe;
		return true;
	}

	public static function findByUsername($username) {
		return self::findOne(['email' => $username]);
	}

	public function getLogin() {
		return $this->email;
	}

	public function getCard() {
		if ($this->userCardId) {
			return $this->hasOne(UserCard::className(), ['id' => 'userCardId']);
		}
		return new UserCard();
	}
}

class UserQuery extends ActiveQuery {

}
<?php

namespace app\models;
use yii\helpers\StringHelper;

/**
 * Class Uploader
 * @package app\models
 *
 * @property int    $id
 * @property string $source
 * @property string $title
 * @property int    $videoId
 */
class VideoDocument extends ActiveRecord {

	public function rules() {
		return [
			[['source', 'videoId'], 'required'],
			[['title'], 'safe'],
		];
	}

	public function beforeDelete() {
		if (parent::beforeDelete()) {
			@unlink(\Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $this->source);
			return true;
		} else {
			return false;
		}
	}

	public function getName() {
		return $this->title?: StringHelper::basename($this->source);
	}
}
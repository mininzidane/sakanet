<?php

use \app\models\User;
use \app\models\Admin;

return [
	Admin::ACTIONS => [
		'type'        => 2,
		'description' => 'Admin actions',
	],
	Admin::ROLE    => [
		'type'     => 1,
		'children' => [
			Admin::ACTIONS,
//			User::ROLE,
		],
	],
	User::ACTIONS  => [
		'type'        => 2,
		'description' => 'User actions',
	],
	User::ROLE     => [
		'type'     => 1,
		'children' => [
			User::ACTIONS,
		]
	],
];

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class BaseController extends Controller {

	public $enableCsrfValidation = false;
	public $layout = 'main';

	/**
	 * Makes array data to view as JSON
	 *
	 * @param array $data
	 * @return string
	 */
	public function renderJson(array $data) {
		header('Content-Type: application/json; charset=utf-8');
		return json_encode($data, defined('JSON_UNESCAPED_UNICODE')? JSON_UNESCAPED_UNICODE: 0);
	}

	public function actionError() {
		if ($error = Yii::$app->errorHandler) {
			if (Yii::$app->request->isAjax) {
				echo $error->exception->getMessage();
			} else {
				try {
					return $this->render("/main/error{$error->exception->statusCode}", ['error' => $error->exception]);
				} catch (\Exception $e) {
					return $this->render('/main/error', ['error' => $error->exception]);
				}
			}
		}

		return $this->render('error', ['message' => 'Unknown error']);
	}
}

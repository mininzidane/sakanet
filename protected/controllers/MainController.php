<?php

namespace app\controllers;

use app\models\User;
use app\models\Admin;
use app\models\RegistrationForm;
use app\models\Video;
use app\models\VideoCategory;
use Yii;
use yii\filters\AccessControl;
use app\models\LoginForm;
use yii\web\HttpException;
use app\models\VideoDocument;

class MainController extends BaseController {

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['login', 'logout', 'index', 'search-by-categories', 'registration', 'error'],
						'allow'   => true,
					],
					[
						'actions' => ['edit-profile'],
						'roles'   => [Admin::ROLE],
						'allow'   => false,
					],
					[
						'roles' => ['@'],
						'allow' => true,
					],
				],
			],
		];
	}

	public function actions() {
		return [
//			'error'   => [
//				'class' => 'yii\web\ErrorAction',
//			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST? 'testme': null,
			],
		];
	}

	public function actionIndex() {
		$newVideos = Video::find()->all();
		$categories = VideoCategory::find()->all();
		return $this->render('index', [
			'newVideos'  => $newVideos,
			'categories' => $categories,
		]);
	}

	public function actionSearchByCategories() {
		$q = Yii::$app->request->getQueryParam('q', []);

		if (array_key_exists('categories', $q)) {
			$categoryIds = $q['categories'];
		} else {
			$categoryIds = [];
		}
		$videos = Video::find()->where(['categoryId' => $categoryIds])->all();
		return $this->render('search-by-categories-results', [
			'videos' => $videos,
		]);
	}

	public function actionVideoDetail($id) {
		$video = Video::find()->where('id = :id', [':id' => $id])->with('parts', 'uploader', 'documents')->one();
		return $this->render('video-detail', [
			'video' => $video,
		]);
	}

	public function actionLogin() {
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	public function actionLogout() {
		Yii::$app->user->logout();

		return $this->goHome();
	}

	public function actionRegistration() {
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$this->view->title = Yii::t('common', 'Registration');

		$model = new RegistrationForm();

		if (Yii::$app->request->isPost) {
			$model->load(Yii::$app->request->post());
			if ($model->validate()) {
				$user = $model->createUser();
				$success = $user->save();
				Yii::$app->session->setFlash('registration', $success);
				if ($success) {
					return $this->redirect(['login']);
				} else {
					$model->addErrors($user->errors);
				}
			}
		}

		return $this->render('registration', [
			'model' => $model,
		]);
	}

	public function actionEditProfile() {
		$this->view->title = Yii::t('common', 'Edit profile');

		/** @var User $user */
		$user = Yii::$app->user->getIdentity();

		if (Yii::$app->request->isPost) {
			$success = false;
			$post = Yii::$app->request->post();
			if ($user->load($post)) {
				$success = $user->save();
			}

			// -- Save card
			$card = $user->card;
			\Stripe\Stripe::setApiKey(Yii::$app->params['stripeSecretKey']);
			// Create a Customer
			$customer = \Stripe\Customer::create([
				'source'      => $card->stripeToken,
				'description' => $user->email,
			]);
			$card->customerId = $customer->id;
			$success          = $card->save(false, ['customerId']);
			$user->userCardId = $card->id;
			$success          = $success && $user->update() !== false;
			// -- -- -- --

			Yii::$app->session->setFlash('registration', $success);
		}

		return $this->render('registration', [
			'model' => $user,
		]);
	}

	public function actionMyPage() {
		$categories = VideoCategory::find()->all();

		return $this->render('my-page', [
			'categories' => $categories,
		]);
	}

	public function actionDownloadDocument($id) {
		$document = VideoDocument::findOne($id);
		if (!$document) {
			throw new HttpException(404, Yii::t('common', 'Not Found'));
		}

		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=\"" . $document->title . "\"");
		readfile(Yii::getAlias('@webroot') . $document->source);
	}
}

<?php

namespace app\controllers;

use app\components\UserIdentity;
use app\models\Admin;
use app\models\Uploader;
use app\models\User;
use app\models\UserRate;
use app\models\UserVideoViewed;
use Yii;
use yii\filters\AccessControl;
use yii\web\HttpException;

class AjaxController extends BaseController {

	public $layout = false;

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['download-document'],
						'roles'   => [Admin::ROLE],
						'allow'   => true,
					],
					[
						'roles' => [Admin::ROLE],
						'allow' => false,
					],
					[
						'roles' => [User::ROLE],
						'allow' => true,
					],
				],
			],
		];
	}

	public function actionSendFeedback() {
		$uploaderId = Yii::$app->request->post('uploaderId');
		$text       = Yii::$app->request->post('text');

		if (!$uploaderId || !$text) {
			throw new HttpException(400);
		}

		$uploader = Uploader::findOne($uploaderId);

		if (!$uploader) {
			throw new HttpException(404);
		}

		$sent = Yii::$app->mailer
			->compose()
			->setTextBody($text)
			->setFrom(Yii::$app->params['emailFrom'])
			->setTo($uploader->mail)
			->setSubject('Send from sakanet')
			->send();

		return (int) $sent;
	}

	public function actionRate($videoId, $value) {
		/** @var UserIdentity $user */
		$user   = Yii::$app->user;
		$userId = $user->getCleanId();

		$rate = UserRate::find()->where([
			'userId'  => $userId,
			'videoId' => $videoId,
		])->one();

		if (!$rate) {
			$rate          = new UserRate();
			$rate->userId  = $userId;
			$rate->videoId = $videoId;
		}
		$rate->rate = $value;
		return (int) $rate->save();
	}

	public function actionSetVideoViewed($videoId) {
		$userId = Yii::$app->user->getCleanId();
		if (UserVideoViewed::findOne([
			'userId'  => $userId,
			'videoId' => $videoId,
		])
		) {
			return 0;
		}
		$videoViewed             = new UserVideoViewed();
		$videoViewed->attributes = [
			'userId'  => $userId,
			'videoId' => $videoId,
		];
		return (int) $videoViewed->save();
	}

	public function actionPayWithCard($amount) {
		/** @var User $user */
		$user = Yii::$app->user->getIdentity();
		if (!$user->userCardId) {
			throw new HttpException(404, Yii::t('common', 'Card not found'));
		}

		$result = $user->card->pay($amount);
		return $result === true? 1: $result;
	}
}

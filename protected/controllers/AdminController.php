<?php

namespace app\controllers;

use app\models\Admin;
use app\models\User;
use app\models\Video;
use app\models\VideoCategory;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use app\models\AdminLoginForm;
use app\models\Uploader;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;

class AdminController extends BaseController {

	public $layout = 'admin';

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['login', 'logout', 'error'],
						'allow'   => true,
					],
					[
						'allow' => true,
						'roles' => [Admin::ROLE],
					],
				],
				'denyCallback' => function($rule, $action) {
					/** @var User $user */
					$user = Yii::$app->user->getIdentity();
					if ($user && $user->role != Admin::ROLE) {
						throw new HttpException(403, Yii::t('common', 'You are not allowed to perform this action'));
					}
					/** @var \yii\base\InlineAction $action */
					return $this->redirect(Url::to(['login']));
				},
			],
		];
	}

	public function actionLogin() {
		if (!\Yii::$app->user->isGuest) {
			return $this->redirect(['index']);
		}

		$model = new AdminLoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->redirect(['index']);
		} else {
			return $this->render('/main/login', [
				'model' => $model,
			]);
		}
	}

	public function actionIndex() {
		return $this->redirect(['uploader-list']);
	}

	public function actionViewerList() {
		$users = User::find()->all();
		return $this->render('viewer-list', [
			'users' => $users,
		]);
	}

	public function actionViewerAdd() {
		$user = new User();

		if (Yii::$app->request->isPost) {
			$user->load(Yii::$app->request->post());
			$success = $user->save();
			Yii::$app->session->setFlash(Yii::$app->controller->action->id, $success?: current($user->getFirstErrors()));
			if ($success) {
				$this->redirect(['viewer-list']);
			}
		}

		return $this->render('viewer-edit', [
			'user' => $user
		]);
	}

	public function actionViewerEdit($id) {
		$user = User::findOne($id);

		if (Yii::$app->request->isPost) {
			$user->load(Yii::$app->request->post());
			$success = $user->save();
			Yii::$app->session->setFlash(Yii::$app->controller->action->id, $success?: current($user->getFirstErrors()));
			if ($success) {
				$this->redirect(['viewer-list']);
			}
		}

		return $this->render('viewer-edit', [
			'user' => $user
		]);
	}

	public function actionUploaderList() {
		$users = Uploader::find()->all();
		return $this->render('uploader-list', [
			'users' => $users,
		]);
	}

	public function actionUploaderAdd() {
		$uploader = new Uploader();
		if (Yii::$app->request->isPost) {
			$uploader->load(Yii::$app->request->post());
			$success = $uploader->save();
			Yii::$app->session->setFlash('uploader', $success);
			if ($success) {
				return $this->redirect(['admin/uploader-list']);
			}
		}
		return $this->render('uploader-edit', [
			'user' => $uploader,
		]);
	}

	public function actionUploaderEdit($id) {
		$uploader = Uploader::findOne($id);
		if (Yii::$app->request->isPost) {
			$uploader->load(Yii::$app->request->post());
			$success = $uploader->save();
			Yii::$app->session->setFlash('uploader', $success);
			if ($success) {
				return $this->redirect(['admin/uploader-list']);
			}
		}
		return $this->render('uploader-edit', [
			'user' => $uploader,
		]);
	}

	public function actionUploaderDetail($id) {
		$uploader = Uploader::find()->with('videos', 'videos.parts')->where('id = :id', [':id' => $id])->one();

		return $this->render('uploader-detail', [
			'user' => $uploader,
		]);
	}

	public function actionVideoAdd($uploaderId) {
		$video = new Video();

		if (Yii::$app->request->isPost) {
			$video->load(Yii::$app->request->post());
			$video->uploaderId = $uploaderId;
			$success           = $video->save();

			if ($success) {
				$success = $video->saveParts();
				$video->saveDocuments();
				Yii::$app->session->setFlash('video', $success);
				if ($success) {
					return $this->redirect(['admin/uploader-detail', 'id' => $uploaderId]);
				}
			}
		}

		return $this->render('video-edit', [
			'model' => $video,
		]);
	}

	public function actionVideoEdit($id) {
		/** @var Video $video */
		$video = Video::find()->where('id = :id', [':id' => $id])->one();

		if (Yii::$app->request->isPost) {
			$video->load(Yii::$app->request->post());
			$success = $video->save();

			if ($success) {
				$success = $video->saveParts();
				$video->saveDocuments();
				Yii::$app->session->setFlash('video', $success);
				if ($success) {
					return $this->redirect('');
				}
			}
		}

		return $this->render('video-edit', [
			'model' => $video,
		]);
	}

	public function actionVideoCategoryList() {
		$categories = VideoCategory::find()->all();
		return $this->render('video-category-list', [
			'categories' => $categories,
		]);
	}

	public function actionVideoCategoryAdd() {
		$category = new VideoCategory();

		if (Yii::$app->request->isPost) {
			$category->load(Yii::$app->request->post());
			$success = $category->save();

			if ($success) {
				Yii::$app->session->setFlash('video-category', $success);
				if ($success) {
					return $this->redirect(['admin/video-category-list']);
				}
			}
		}

		return $this->render('video-category-edit', [
			'model' => $category,
		]);
	}

	public function actionVideoCategoryEdit($id) {
		$category = VideoCategory::findOne($id);

		if (Yii::$app->request->isPost) {
			$category->load(Yii::$app->request->post());
			$success = $category->save();

			if ($success) {
				Yii::$app->session->setFlash('video-category', $success);
				if ($success) {
					return $this->redirect(['admin/video-category-list']);
				}
			}
		}

		return $this->render('video-category-edit', [
			'model' => $category,
		]);
	}

	public function actionModelDelete($id, $model = 'user', $redirect = ['admin/viewer-list']) {
		$model   = ucfirst($model);
		$class   = '\app\models\\' . $model;
		$success = $class::findOne($id)->delete();
		Yii::$app->session->setFlash(strtolower($model), $success);
		return $this->redirect($redirect);
	}
}

<?php

class m160816_142340_create_user_video_paid extends \yii\db\Migration {

	public function safeUp() {
		$this->createTable('user_video_paid', [
			'userId'  => 'INT(11) NOT NULL',
			'videoId' => 'INT(11) NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (userId, videoId)'
		]);
	}

	public function safeDown() {
		$this->dropTable('user_video_paid');
	}
}

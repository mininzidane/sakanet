<?php

use yii\db\Migration;

/**
 * Handles adding title to table `video_part`.
 */
class m160719_163051_add_title_to_video_part extends Migration {

	/**
	 * @inheritdoc
	 */
	public function up() {
		$this->addColumn(\app\models\VideoPart::tableName(), 'title', 'VARCHAR(128) NULL DEFAULT NULL AFTER id');
	}

	/**
	 * @inheritdoc
	 */
	public function down() {
		$this->dropColumn(\app\models\VideoPart::tableName(), 'title');
	}
}

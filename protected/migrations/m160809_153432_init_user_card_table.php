<?php

class m160809_153432_init_user_card_table extends \yii\db\Migration {

	public function safeUp() {
		$this->createTable(\app\models\UserCard::tableName(), [
			'id'         => $this->primaryKey(),
			'customerId' => 'VARCHAR(64) NOT NULL',
			'created'    => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
		$this->addColumn(\app\models\User::tableName(), 'userCardId', 'INT(11) NULL DEFAULT NULL AFTER password');
		$this->alterColumn(\app\models\User::tableName(), 'subscribe', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1');
	}

	public function safeDown() {
		$this->alterColumn(\app\models\User::tableName(), 'subscribe', 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
		$this->dropColumn(\app\models\User::tableName(), 'userCardId');
		$this->dropTable(\app\models\UserCard::tableName());
	}
}

<?php

class m160802_060232_add_default_superadmin extends \yii\db\Migration {

	public function safeUp() {
		$this->insert(\app\models\Admin::tableName(), ['login' => 'admin', 'password' => '59f8c4fa7d66512b75087a71637f36f1']); // admin/1234
	}

	public function safeDown() {
		$this->delete(\app\models\Admin::tableName(), "login = 'admin' and password = '59f8c4fa7d66512b75087a71637f36f1'");
	}
}

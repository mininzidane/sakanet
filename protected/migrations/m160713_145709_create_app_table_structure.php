<?php

use yii\db\Migration;

/**
 * Handles the creation for table `app_table_structure`.
 */
class m160713_145709_create_app_table_structure extends Migration {

	/**
	 * @inheritdoc
	 */
	public function up() {
		$this->dropColumn('user', 'role');
		$this->createTable('admin', [
			'id'       => $this->primaryKey(),
			'login'    => 'VARCHAR(32) NOT NULL',
			'password' => 'VARCHAR(64) NULL DEFAULT NULL',
			'created'  => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
		$this->createTable('uploader', [
			'id'          => $this->primaryKey(),
			'title'       => 'VARCHAR(64) NULL DEFAULT NULL',
			'contactName' => 'VARCHAR(64) NULL DEFAULT NULL',
			'mail'        => 'VARCHAR(64) NULL DEFAULT NULL',
			'phone'       => 'VARCHAR(32) NULL DEFAULT NULL',
			'address'     => 'VARCHAR(128) NULL DEFAULT NULL',
			'created'     => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
		$this->createTable('video', [
			'id'          => $this->primaryKey(),
			'title'       => 'VARCHAR(128) NOT NULL',
			'description' => 'TEXT NULL DEFAULT NULL',
			'uploaderId'  => 'INT(11) NOT NULL',
			'payment'     => 'FLOAT NOT NULL DEFAULT 0',
			'rating'      => 'FLOAT NOT NULL DEFAULT 1',
			'created'     => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
		$this->createTable('video_part', [
			'id'       => $this->primaryKey(),
			'source'   => 'VARCHAR(256) NOT NULL',
			'duration' => 'INT(5) NOT NULL',
			'videoId'  => 'INT(11) NOT NULL',
			'created'  => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
		$this->createTable('video_document', [
			'id'      => $this->primaryKey(),
			'source'  => 'VARCHAR(256) NOT NULL',
			'videoId' => 'INT(11) NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function down() {
		$this->dropTable('video_document');
		$this->dropTable('video_part');
		$this->dropTable('video');
		$this->dropTable('uploader');
		$this->dropTable('admin');
		$this->addColumn('user', 'role', 'VARCHAR(32) NOT NULL DEFAULT \'viewer\'');
	}
}

<?php

use app\models\User;
use app\models\VideoPart;
use app\models\VideoDocument;

class m160818_112319_change_date_of_birth_add_video_preview extends \yii\db\Migration {

	public function safeUp() {
		$this->dropColumn(User::tableName(), 'year');
		$this->dropColumn(User::tableName(), 'month');
		$this->dropColumn(User::tableName(), 'day');
		$this->addColumn(User::tableName(), 'birthday', 'VARCHAR(16) NULL DEFAULT NULL AFTER occupation');
		$this->addColumn(VideoPart::tableName(), 'preview', 'VARCHAR(256) NULL DEFAULT NULL AFTER source');
		$this->addColumn(VideoDocument::tableName(), 'title', 'VARCHAR(32) NULL DEFAULT NULL AFTER source');
	}

	public function safeDown() {
		$this->dropColumn(VideoDocument::tableName(), 'title');
		$this->dropColumn(VideoPart::tableName(), 'preview');
		$this->dropColumn(User::tableName(), 'birthday');
		$this->addColumn(User::tableName(), 'day', 'INT(2) NULL DEFAULT NULL');
		$this->addColumn(User::tableName(), 'month', 'INT(2) NULL DEFAULT NULL');
		$this->addColumn(User::tableName(), 'year', 'INT(4) NULL DEFAULT NULL');
	}
}

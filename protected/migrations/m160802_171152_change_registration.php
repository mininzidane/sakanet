<?php

class m160802_171152_change_registration extends \yii\db\Migration {

	public function safeUp() {
		$this->dropColumn('user', 'lastName');
		$this->dropColumn('user', 'lastNameWrite');
	}

	public function safeDown() {
		$this->addColumn('user', 'lastNameWrite', 'VARCHAR(128) NULL DEFAULT NULL');
		$this->addColumn('user', 'lastName', 'VARCHAR(128) NULL DEFAULT NULL');
	}
}

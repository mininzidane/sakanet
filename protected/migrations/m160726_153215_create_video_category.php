<?php

use yii\db\Migration;

/**
 * Handles the creation for table `video_category`.
 */
class m160726_153215_create_video_category extends Migration {

	/**
	 * @inheritdoc
	 */
	public function up() {
		$this->createTable('video_category', [
			'id'      => $this->primaryKey(),
			'title'   => 'VARCHAR(64) NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
		$this->addColumn(\app\models\Video::tableName(), 'categoryId', 'INT(11) NULL DEFAULT NULL AFTER description');
	}

	/**
	 * @inheritdoc
	 */
	public function down() {
		$this->dropColumn(\app\models\Video::tableName(), 'categoryId');
		$this->dropTable('video_category');
	}
}

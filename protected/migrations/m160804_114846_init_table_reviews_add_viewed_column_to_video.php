<?php

class m160804_114846_init_table_reviews_add_viewed_column_to_video extends \yii\db\Migration {

	public function safeUp() {
		$this->createTable('user_rate_to_video', [
			'id'      => $this->primaryKey(),
			'userId'  => 'INT(11) NOT NULL',
			'rate'    => 'TINYINT(1) NOT NULL DEFAULT 1',
			'videoId' => 'INT(11) NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
		$this->createTable('user_video_viewed', [
			'userId'  => 'INT(11) NOT NULL',
			'videoId' => 'INT(11) NOT NULL',
			'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'PRIMARY KEY (userId, videoId)'
		]);
	}

	public function safeDown() {
		$this->dropTable('user_video_viewed');
		$this->dropTable('user_rate_to_video');
	}
}

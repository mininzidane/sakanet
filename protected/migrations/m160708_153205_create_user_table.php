<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user_table`.
 */
class m160708_153205_create_user_table extends Migration {

	/**
	 * @inheritdoc
	 */
	public function up() {
		$this->createTable('user', [
			'id'             => $this->primaryKey(),
			'firstName'      => 'VARCHAR(128) NULL DEFAULT NULL',
			'lastName'       => 'VARCHAR(128) NULL DEFAULT NULL',
			'firstNameWrite' => 'VARCHAR(128) NULL DEFAULT NULL',
			'lastNameWrite'  => 'VARCHAR(128) NULL DEFAULT NULL',
			'email'          => 'VARCHAR(128) NULL DEFAULT NULL',
			'password'       => 'VARCHAR(64) NULL DEFAULT NULL',
			'zip'            => 'VARCHAR(64) NULL DEFAULT NULL',
			'prefecture'     => 'VARCHAR(128) NULL DEFAULT NULL',
			'city'           => 'VARCHAR(64) NULL DEFAULT NULL',
			'address'        => 'VARCHAR(128) NULL DEFAULT NULL',
			'occupation'     => 'VARCHAR(128) NULL DEFAULT NULL',
			'year'           => 'INT(4) NULL DEFAULT NULL',
			'month'          => 'INT(2) NULL DEFAULT NULL',
			'day'            => 'INT(2) NULL DEFAULT NULL',
			'subscribe'      => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
			'role'           => 'VARCHAR(32) NOT NULL DEFAULT \'viewer\'',
			'created'        => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function down() {
		$this->dropTable('user');
	}
}

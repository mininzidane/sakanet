<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Bower installed js/css files.
 */
class BowerAsset extends AssetBundle {

	public $sourcePath = '@bower';
	public $js         = [
//		'backbone/backbone.js',
//		'underscore/underscore.js',
		'bootstrap/dist/js/bootstrap.min.js',
		'fancybox/source/jquery.fancybox.pack.js?v=2.1.5',
		'jquery.rateit/scripts/jquery.rateit.min.js',
		'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
		'bootstrap-datepicker/dist/locales/bootstrap-datepicker.ja.min.js',
	];
	public $css        = [
//		'jquery-ui/themes/ui-lightness/jquery-ui.min.css',
		'fancybox/source/jquery.fancybox.css?v=2.1.5',
		'jquery.rateit/scripts/rateit.css',
		'bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
	];
}

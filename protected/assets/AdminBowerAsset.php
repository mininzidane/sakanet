<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Asset bundle for the Bower installed js/css files.
 */
class AdminBowerAsset extends AssetBundle {

	public $sourcePath = '@bower';
	public $js = [
		'bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
		'bootstrap-datepicker/dist/locales/bootstrap-datepicker.ja.min.js',
	];
	public $css = [
		'bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
	];
}

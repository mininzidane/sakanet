<?php

return [
	'/'                       => 'main/index',
	'/admin/'                 => 'admin/index',
	'/<action>/'              => 'main/<action>', // common
	'/<controller>/<action>/' => '<controller>/<action>',
];
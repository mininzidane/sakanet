<?php

defined('APPLICATION_MODE') or define('APPLICATION_MODE', isset($_SERVER['APPLICATION_MODE'])? $_SERVER['APPLICATION_MODE']: 'production');
$configFileName = realpath(__DIR__ . '/env.php');

Yii::setAlias('@tests', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'tests');
Yii::setAlias('@cli', dirname(__DIR__) . DIRECTORY_SEPARATOR . '/cli');

$config = array_replace_recursive(
	require($configFileName), [
	'id'                  => 'basic-console',
	'basePath'            => dirname(__DIR__),
	'bootstrap'           => ['log', 'gii'],
	'controllerNamespace' => 'app\commands',
	'modules'             => [
		'gii' => 'yii\gii\Module',
	],
	'components'          => [
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'log'   => [
			'targets' => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
	],
	'controllerMap'       => [
		'migrate' => [
			'class'        => 'yii\console\controllers\MigrateController',
			'templateFile' => '@app/migrations/template.php',
		],
		// insert your commands here like migrate command
	],
	'params'              => [],
]);

return $config;

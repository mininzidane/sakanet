<?php

$config = [
	'id'             => 'skeleton',
	'basePath'       => dirname(__DIR__),
	'bootstrap'      => ['log'],
	'sourceLanguage' => 'ja',
	'language'       => 'ja',
	'components'     => [
		'request'      => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'cF-8JH7_9L7SZIDGEC0RbpEPUtg9PfJ0',
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
		'user'         => [
			'class'           => 'app\components\UserIdentity',
			'identityClass'   => 'app\models\User',
			'loginUrl'        => ['main/login'],
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'main/error',
		],
		'mailer'       => [
			'class' => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
//			'useFileTransport' => true,
		],
		'log'          => [
			'traceLevel' => YII_DEBUG? 3: 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'urlManager'   => [
			'cache'               => null,
			'suffix'              => '/',
			'enableStrictParsing' => true,
			'class'               => 'yii\web\UrlManager',
			'enablePrettyUrl'     => true,
			'showScriptName'      => false,
			'rules'               => require('_routes.php'),
		],
		'view'         => [
			'class' => 'yii\web\View',
		],
		'authManager'  => [
			'class' => 'yii\rbac\PhpManager',
		],
		'i18n'         => [
			'translations' => [
				'*' => [
					'class'            => 'yii\i18n\PhpMessageSource',
					'basePath'         => "@app/messages",
					'forceTranslation' => true,
				],
			],
		],
		'image'        => [
			'class'  => 'yii\image\ImageDriver',
			'driver' => 'GD',  //GD or Imagick
		],
	],
	'aliases'        => [
		'@views' => '@app/views',
		'@parts' => '@views/_parts',
	],
	'params'         => [
		'videoFolder'        => DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR,
		'emailFrom'          => 'no-reply@sakanet',
		'stripePublicKey'    => 'pk_test_6pRNASCoBOKtIshFeQd4XMUh',
		'stripeSecretKey'    => 'sk_test_BQokikJOvBiI2HlWgH4olfQ2',
		'previewImageWidth'  => 200,
	],
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][]      = 'debug';
	$config['modules']['debug'] = 'yii\debug\Module';

	$config['bootstrap'][]    = 'gii';
	$config['modules']['gii'] = 'yii\gii\Module';
}

return $config;

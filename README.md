Yii 2 Basic Application Template
================================

Yii 2 Basic Application Template is a skeleton Yii 2 application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.


DIRECTORY STRUCTURE
-------------------

      protected/assets/             contains assets definition
      protected/commands/           contains console commands (controllers)
      protected/config/             contains application configurations
      protected/controllers/        contains Web controller classes
      protected/mail/               contains view files for e-mails
      protected/models/             contains model classes
      protected/runtime/            contains files generated during runtime
      protected/tests/              contains various tests for the basic application
      protected/vendor/             contains dependent 3rd-party packages
      protected/views/              contains view files for the Web application
      public/                       contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

## Clone from GitLab
First you need to clone yii2-skeleton repo from GitLab, then you need to change repo url to commit to your new project repo, not to skeleton repo (note that skeleton url may differ from example below):
```bash
git clone git@gitlab.kama.gs:php-extensions/yii2.git [PROJECT ROOT FOLDER NAME]
git remote set-url origin [GIT REMOTE URL]
```

## Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

All you need is to run next commands:
```bash
cd /path/to/project
# Install composer dependencies
composer install
```

## Multiple configuration files
Use APPLICATION_MODE = dev for development environment, it only needed for Yii2 internal debugger.
Use env.php to use your own configuration: @see protected/config/env.php (file is not under version control).

## File structure
There are directories under .gitignore that must be created before application is started:
- public/assets
- protected/runtime

## Install frontend packages
Use [Bower](http://bower.io/) to install web packages:
```bash
cd /path/to/project
bower install
```

## Run migration tool
```bash
# Run migration
cd /path/to/project
./protected/yii migrate
```

## App config
App mode is set at nginx config (see below): dev or production.
Yii shows debug toolbar for app_mode = dev.
For platform specified config use protected/config/env.php that contains something like (without []):
```php
return [
	'components' => [
		'db' => [
			'class'    => 'yii\db\Connection',
			'dsn'      => 'mysql:host=localhost;dbname=[db_name]',
			'username' => '[user_name]',
			'password' => '[password]',
			'charset'  => 'utf8',
		],
	],
	'params'     => [

	],
];
```

## Configure nginx
### domain.com.conf
```nginx
server {
	listen 80;
	server_name domain.com www.domain.com;

	access_log log/nginx/domain.com-access.log;
	error_log log/nginx/domain.com-error.log;

	set $www_root /opt/domain.com/public;
	set $app_mode production; # use dev for development

	# Всех пришедших на домен не по адресу domain.com редиректим на него
	if ($host !~ ^domain.com) {
		rewrite ^(.*)$ http://domain.com$1 permanent;
	}

	include include/yii.conf;
}
```

### include/yii.conf
```nginx
root $www_root;

charset utf-8;
set $bootstrap index.php;
index index.html $bootstrap;

location ~ /\. {
	deny all;
}

location ~ ^(.+\.(js|css|jpg|gif|png|ico|swf|mp3|html|eot|woff|ttf|svg|zip|pdf))$ {
	try_files $uri /$bootstrap?$args;
}

location ~ .* {
	set $fsn /$bootstrap;
	if (-f $document_root$fastcgi_script_name){
		set $fsn $fastcgi_script_name;
	}

	include fastcgi.conf;
	fastcgi_pass    php-fpm;

	fastcgi_param   SCRIPT_FILENAME         $document_root$fsn;
	fastcgi_param   PATH_INFO               $fastcgi_path_info;
	fastcgi_param   PATH_TRANSLATED         $document_root$fsn;
	fastcgi_param   APPLICATION_MODE        $app_mode;
}
```

FRONTEND DEVELOPMENT
--------------------

It is used Gulp at frontend. Look /public/gulpfile.js to understand how it works. The following commands are necessary to develop frontend:
```bash
# Globally install Gulp (do it once if you did not before)
npm install -g gulp
# Install local Node dependencies before you can use Gulp
npm install
# Run sass compiler. Keep sass files in /css/sass, compiled files are in /css/sass/compiled
gulp sass
# Run to concat appropriate files
gulp js
gulp css
# Default command is running the both commands
gulp
# Prod command will minify concated css and js files
gulp prod
```
